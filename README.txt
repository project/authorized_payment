CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Configuration
 * Maintainers


INTRODUCTION
------------
This module provides integration between your Drupal website and the powerful Authorize.Net payment gateway.
using this module user can payment the using creadit card. 

the Webform Authorized Payment module, you can harness the reliability and security of Authorize.Net's payment processing capabilities directly within your Drupal site.


REQUIREMENTS
------------
This module requires the following:
* Webform 
* An account with Authorize.Net
* If you want to use Authorize.Net in test mode, then you'll need to sign up
  for an Authorize.Net sandbox account - https://developer.authorize.net/hello_world/sandbox.html



CONFIGURATION
-------------

Go to Home >> Structure >> Webform >> Authorize.net Configuration .

Authorize.net Configuration : set the API Login ID and TRANSACTION KEY in Authorize.net Configuration Form.

enable the payment setting on webform follow the below steps.

select webform you'd like to use for payment.

1) Go to Home >> Structure >> Webform >> Example webform >> build >> settings >> Form.
2) check the checkbox  "Authorization Payment Field" at the bottom of the page.
3) click on save button.


MAINTAINERS
-----------

 Dimple (Kudosintech) - https://www.drupal.org/u/dimplel
 Maitri (Kudosintech) - https://www.drupal.org/u/maitri_2112
 Sadab (Kudosintech)  - https://www.drupal.org/u/shaikh-sadab
