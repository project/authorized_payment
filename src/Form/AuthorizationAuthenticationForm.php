<?php

namespace Drupal\webform_authorize_net_payment\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Authorize.Net Webform Payment settings for this site.
 */
class AuthorizationAuthenticationForm  extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_authorize_net_payment_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['webform_authorize_net_payment.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('webform_authorize_net_payment.settings');

    $form['api_login_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Login ID'),
      '#description' => t('Enter your Authorize.Net API Login ID - NOT your username.'),
      '#weight' => -1,
      '#required' => TRUE,
      '#default_value' => \Drupal::state()->get('api_login_id', \Drupal::config('webform_authorize_net_payment.settings')->get('api_login_id')),
    );

    $form['transaction_key'] = array(
      '#type' => 'password',
      '#title' => t('Transaction key'),
      '#description' => t('This field does not display your transaction key. If you have already set a transaction key previously, please leave this field empty unless you intend to update it.'),
      '#required' => TRUE,
      '#default_value' => \Drupal::state()->get('transaction_key', \Drupal::config('webform_authorize_net_payment.settings')->get('transaction_key')),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('webform_authorize_net_payment.settings')
    ->set('api_login_id', $form_state->getValue('api_login_id'))
    ->set('transaction_key', $form_state->getValue('transaction_key'))
    ->save();
    parent::submitForm($form, $form_state);    
  }
}