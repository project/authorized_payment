<?php

namespace Drupal\webform_authorize_net_payment\Controller;
define("AUTHORIZENET_LOG_FILE", "phplog");
use Drupal\Core\Controller\ControllerBase;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use Drupal\Core\Url;
use Drupal\Core\Link;


class AuthorizeNetWebformPaymentController extends ControllerBase {
    function chargeCreditCard($data)
    {

        
        $amount = $data["payment_amount"];

        /** 
         *  Create a merchantAuthenticationType object with authentication details
         * retrieved from the constants file
         */
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(\Drupal::config('webform_authorize_net_payment.settings')->get('api_login_id'));
        $merchantAuthentication->setTransactionKey(\Drupal::config('webform_authorize_net_payment.settings')->get('transaction_key'));

        /**
         * Set the transaction's refId
         */ 
        $refId = 'ref' . time();

       
        /**
         * Create the payment data for a credit card
         */
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($data['card_number']);
        $creditCard->setExpirationDate($data['expiration_date']);
        $creditCard->setCardCode($data["cvv"]);

        /** 
         * Add the payment data to a paymentType object
        */
        $paymentType = new AnetAPI\PaymentType();
        $paymentType->setCreditCard($creditCard); 

        /**
         * Set the customer's Bill To address
         */
        $customerAddress = new AnetAPI\CustomerAddressType();
        $customerAddress->setFirstName($data['first_name']);
        $customerAddress->setLastName($data['last_name']);

        /**
         * Create a TransactionRequestType object and add the previous objects to it
         */
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($amount);
        $transactionRequestType->setBillTo($customerAddress);
        $transactionRequestType->setPayment($paymentType);

        /**
         * Assemble the complete transaction request
         */
        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequestType);

        /**
         * Create the controller and get the response
        */ 
        $controller = new AnetController\CreateTransactionController($request);   
        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        $cardDetailsWithStatus = [];

        
        \Drupal::logger('$response')->debug('<pre><code>' . print_r($response, TRUE) . '</code></pre>');
        /**
         *  Check to see if the API request was successfully received and acted upon
         */
        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {

            /**
             * Since the API request was successful, look for a transaction response
             * and parse it to display the results of authorizing the card
             */
            $tresponse = $response->getTransactionResponse();            
            if ($tresponse != null && $tresponse->getMessages() != null) { 
                /**
                 *   Transaction info 
                 */
                $transaction_id = $tresponse->getTransId(); 
                $payment_status = $response->getMessages()->getMessage()[0]->getText();                
                $cvv = $data["cvv"];
                $cvvMasked = substr_replace($cvv, str_repeat('*', strlen($cvv)), 0);
                $masked = substr($data['card_number'], -4);     
                $cardExpiration = $data['expiration_date'];
                $expirationDateMasked = substr_replace($cardExpiration, '**', 3, 2);
                $cardDetailsWithStatus['card_number'] = $masked;
                $cardDetailsWithStatus['cvv'] = $cvvMasked;
                $cardDetailsWithStatus['expiration_date'] = $expirationDateMasked;
                $cardDetailsWithStatus['transaction_id'] = $transaction_id;
                $cardDetailsWithStatus['Payment_status'] = $payment_status;               
            }     
            return $cardDetailsWithStatus;
        } 
        else if(empty($response->getRefId())){
            $current_user = \Drupal::currentUser();
            $roles = $current_user->getAccount()->getRoles();
            if($roles[1] === 'administrator'){                   
                $routeName = 'webform_authorize_net_payment.form';
                $url = Url::fromRoute($routeName); 
                /**
                 *  Create a Link object using the URL and link text.
                 */
                $linkText = 'Click here';
                $link = Link::fromTextAndUrl($linkText, $url);
                /**
                 * Generate the renderable link element.
                 */
                $renderableLink = $link->toRenderable();
                /**
                 * Render the link element as HTML.
                 */
                $output = \Drupal::service('renderer')->render($renderableLink);
                $cardDetailsWithStatus =  'Please fill authorize Configuration form fields on given link '.$output;
                return [
                '#markup' => $cardDetailsWithStatus,
                ];                   
            }
            elseif($roles[0] == 'authenticated'){
                $cardDetailsWithStatus =  'Payment field is Disabled. For processing payment please contact to administrator';
            }
            else{
                $cardDetailsWithStatus =  'Payment field is Disabled. For processing payment please contact to administrator';                                           
            }
            return $cardDetailsWithStatus;          
        } 
        else {
            /**
             * Or, print errors if the API request wasn't successful
             */
            $cardDetailsWithStatus = $response->getTransactionResponse()->getErrors()[0]->getErrorText();           
            return $cardDetailsWithStatus;
        }
    }   
}
